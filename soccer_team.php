<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://tympanus.net/Development/HoverEffectIdeas/fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="soccer.css" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<header>

    <div class="header">
        <ul class='social-icons'>
            <li> Login</li>
            <li> Register</li>
            <li><i class="fa fa-twitter" aria-hidden="true"></i></li>
            <li><i class="fa fa-facebook" aria-hidden="true"></i></li>
            <li><i class="fa fa-google-plus" aria-hidden="true"></i></li>
            <li><i class="fa fa-chrome" aria-hidden="true"></i></li>
            <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
        </ul>
    </div>
</header>

<section class="top-nav">
    <div>
        Logo Here
    </div>
    <input id="menu-toggle" type="checkbox" />
    <label class='menu-button-container' for="menu-toggle">
        <div class='menu-button'></div>
    </label>
    <ul class="menu">
        <li>Home</li>
        <li>About</li>
        <li>Gallary</li>
        <li>Products</li>
        <li>Contanct</li>

    </ul>
</section>

<div class="heading-picture">
    <img src="https://somersetmall.co.za/img/coffee_feb171.jpg" />

</div>

<div class="container">
    <div class="head">
        <p>Trading hours</p>

    </div>

    <div class="head__description">

        <h1>Khanyo Pantry </h1>
        <p>About Us </p>
        <p>ablished in 2008, The Gourmet Pantry is a wholesale bakery in Cape Town producing a range of quality breads, cakes, pies, quiche, muffins, etc.</p>
        <p>Our clients include Hotels & Conference Centers, Restaurants, Coffee Shops, Delis, Caterers and also available to the public at our coffee shop in Bergvliet.</p>
        <p>We delivery daily in Cape Town & the Southern Suburbs.</p>
        <p>Retail</p>

        <p> For retail sales please visit The Pantry in Bergvliet - our new coffee shop! Shop 3, Glen Alpine Centre, Ladies MIle Road, Bergvliet, Cape Town</p>

        <div class="col-x1">
            <div class="col-x3 ">
                <div class="col-xs-12 col-sm-6 item">
                    <img src="http://www.flatrockwoodfired.com/bakery/backgrounds/bakery-3.jpg" class="img" />
                    <div class="share-links">
                        <a href="#">Order now</i></a>

                    </div>
                </div>

            </div>
            <div class="col-x3">
                <div class="col-xs-12 col-sm-6 item">
                    <img src="http://www.flatrockwoodfired.com/bakery/backgrounds/bakery-3.jpg" class="img" />
                    <div class="share-links">
                        <a href="#">Order now</i></a>

                    </div>
                </div>

            </div>
            <div class="col-x3">
                <div class="col-xs-12 col-sm-6 item">
                    <img src="https://static1.squarespace.com/static/53c7ef0ce4b0711582f384ca/54eab9ede4b05860a4728ff0/552127a6e4b066aeb4b48be0/1487559551067/five_daughters-food-0017.jpg?format=1500w" / class="img">
                    <div class="share-links">
                        <a href="#">Order now</i></a>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="last">

        <p>Bradbury Horton Bakery was founded in 2000 with a focus on producing baked goods with a medium to long shelf life (think frozen waffles or scones). Over the years, our offering has expanded to include ambient products, such as biscuits and a delectable selection of tea loaves, but the focus on a long shelf life has remained the same.</p>
        <p>
            We are privileged to have a loyal and diverse customer base, including South Africa’s biggest supermarket chains, restaurants, caterers, other bakeries and many hardworking and enterprising individuals who supplement their regular income by selling our products in their local communities. We strive to provide every customer with the same high-quality product and high-quality customer service, whether they are big or small.</p>

    </div>