<head>
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web:900" rel="stylesheet" />
    <link rel="stylesheet" href="books.css" />
</head>

<div class="mobile__search">
    <input type="text" placeholder="Find your destination" />
</div>

<section class="product__gallery">
    <h2>My inspirational books</h2>
    <div class="row">
        <div class="grid-6">
            <img src="images/html.jpg" alt="" />
            <button>Design</button>
        </div>
        <div class="grid-6">
            <img src="images/script.jpg" alt="" />
            <button>design</button>
        </div>
        <div class="grid-6">
            <img src="images/java.jpg" alt="" />
            <button>design</button>
        </div>
        <div class="grid-6">
            <img src="images/float.jfif" alt="" />
            <button>design</button>
        </div>
        <div class="grid-6">
            <img src="images/get.jfif" alt="" />
            <button>design</button>
        </div>
        <div class="grid-6">
            <img src="images/eloq.png" alt="" />
            <button>design</button>
        </div>
    </div>

    <section class="categories">
        <h2>Education and work expirience</h2>
        <p>Hover over for details</p>
        <div class="cat__row">
            <div class="grid-3">
                <div class="tour">
                    <img class="tours" src="images/CCT.jpg" alt="" />
                    <div class="tour__description">
                        <p>Big and flashy, subtle and sophisticated, or homespun and disarmingly familiar, America has it all... a country where in the west so admired Celebrity and Pop Culture exists side by side with untouched wilderness. With so many cities and natural wonders spread across 8 million square kilometres of freedom, it's no surprise that the United States are the birthplace of the modern road trip. Few places on Earth can claim the variety, scope and scale of the United States, and its melting pot of influences from around the world has made for a culture rife with – and proud of – its contradictions.</p>

                    </div>
                </div>
            </div>
            <div class="grid-3">
                <div class="tour">
                    <img class="tours" src="images/tshwane.png" alt="" />
                    <div class="tour__description">
                        <p> Information Technology NQF Level 4 | March 2015 – January 2016
                            <br/> Subjects:
                            <br/> Mathematics
                            <br/> Office Data Processing
                            <br/> Computer Programming
                            <br/> English First Additional Language
                            <br/> Life Orientation
                            <br/> Systems Analysis and Design
                            <br/> Communication and Networking
                            <br/>
                            <br/>
                        </p>

                    </div>
                </div>
            </div>
            <div class="grid-3">
                <div class="tour">
                    <img class="tours" src="images/Roy-Logo.jpg" alt="" />
                    <div class="tour__description">
                        <p> National Certificate: Information Technology (Systems Development) | March 2016 – January 2017
                            <br/> Subjects:
                            <br/> Web development
                            <br/> Communication in IT project
                            <br/> Introduction IT in Business
                            <br/> IS Design and Development (PHP)
                            <br/> IS Design and Development (cost + time)
                            <br/> Error detection and testing
                            <br/> Introduction to database and Dynamic web dev 2
                            <br/>

                        </p>

                    </div>
                </div>
            </div>

        </div>
        <div class="cat__row">
            <div class="grid-3">
                <div class="tour">
                    <img class="tours" src="images/mirum.jfif" alt="" />
                    <div class="tour__description">
                        <p>Front End Internship for 3 months with Mirum Agency Cape Town under the supervision of Kapeesh MAnial my manager he gave us work to do, we had to recode the sites that they were working for like </p>

                    </div>
                </div>
            </div>
            <div class="grid-3">
                <div class="tour">
                    <img class="tours" src="images/old-mutual.jpg" alt="" />
                    <div class="tour__description">
                        <p>configure follow me printers, verify/install print Que And set default Multifunctional Devices on workstation Train users how to release print jobs USB / LPT workstation Client Remove redundant cue name Soft and technical skills
                        </p>

                    </div>
                </div>
            </div>
            <div class="grid-3">
                <div class="tour">
                    <img class="tours" src="images/cape-town.jpg" alt="" />
                    <div class="tour__description">
                        <p>I Was working with this Tourism company as intern hosting people who join Us on board and also explain where needs be when they did not get what was said on the commentary
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>