<link rel="stylesheet" href="food.css" />
<h1>My Photoshop Designs</a></h1>

<section class="responsive">
    <h2>Portfolio</h2>
    <ul class="grid-container">
        <li> <img src="images/about-Recovered[1].png">
            <h3>About Page</h3>
            <p>In the real world, you don't know how long the content will be. Writers will write what they need. Photos will be cropped to the shape that they should be. That's good. Make a system that allows this. You want it to be flexible and robust. </p>
        </li>
        <li>
            <img src="images/Gallary-Recovered[1].png">
            <h3>Gallery Page</h3>
            <p>In the real world, you don't know how long the content will be. Writers will write what they need. Photos will be cropped to the shape that they should be.
        </li>
        <li>
            <img src="images/Untitled-1-Recovered[1].png">
            <h3>Portfolio page</h3>
            <p>In the real world, you don't know how long the content will be. Writers will write what they need. </p>
        </li>
        <li>
            <img src="images/Contact-Recovered[1].png">
            <h3>Contact Page</h3>
            <p>In this Page I was creating a Contact page for a Task that i was asked By my Manager during my internship we had to finish a full site in designs.</p>
        </li>
    </ul>
</section>