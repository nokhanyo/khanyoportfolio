<head>
    <link rel="stylesheet" href="index_profil.css" />
    <link rel="stylesheet" href="food.css" />
</head>
<html>
<div id="content_area">

    <section>

        <div class="wrapper">
            <div class="col-x2 ">
                <img src="images/talk-code.png" />

            </div>
            <div class="col-x2">
                <p class="about__intro">
                    Hi, I'm Khanyo Funani, an aspiring web developer with an interest in UX/UI design.
                    <br> I Will be graduating on the 09th of December 2017 in National Certificate: Information Technology (Systems Development)
                    <br> I was born in the Eastern Cape (Tsolo) and raised in Cape Town.

                    <br> Where I finished my Primary then High I then developed passion for Computers thats when I went to a College to do my Information Technology Course, took the networking side and did CISCO Specialist after that then I got interested in the development side where I then applied for a learnership in National Certificate: Information Technology (Systems Development) NQF Level 5 I love my family a lot.
                    <a href="book_shelf.php">
                        <br>
                        <button> See more</button>
                    </a>
                </p>

            </div>
        </div>
    </section>

    <div class="slantedA">

        <div class="block-1">
            <ul id="da-thumbs" class="da-thumbs">
                <li>
                    <a href="https://codepen.io/Khanyo_Funani/pen/aJXxQv?editors=1100">

                        <div>
                            <h2>CSS </h2>
                            <img src="images/pic3.png" width="150" height="150" />
                            <b href="" class="item-btn">View Work</b></div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-2">
            <ul id="da-thumbs" class="da-thumbs">
                <li>
                    <a href="https://codepen.io/Khanyo_Funani/pen/mWqyMK?editors=1100">

                        <div>
                            <h2>HTML </h2>
                            <img src="images/pic1.png" height="150" width="150" /><b/>

                            <b href="" class="item-btn">View Work</b></div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="block-3">
            <ul id="da-thumbs" class="da-thumbs">
                <li>
                    <a href="soccer_team.php">

                        <div>
                            <h2>JavaScript </h2>
                            <img src="images/pic2.png" height="150" width="150" />
                            <b href="" class="item-btn">View Work</b></div>
                    </a>
                </li>
            </ul>
        </div>

    </div>
    <section class="responsive">
        <h2>Just me</h2>
        <ul class="grid-container">
            <li> <img src="images/khanyo.jpg">
                <h3>Khanyo Funani</h3>
                <p>I am diligent hardworking individual. I am analytical, driven, a team player, I work well under pressure and I am extremely pro-active. I am looking for an opportunity develop my skills, to prove myself and realize my full potential. I believe that success is about determination, passion and confidence. </p>
            </li>
            <li>
                <img src="images/mom-aunt.jpg">
                <h3>Mom and Family</h3>
                <p>My Mom and My Aunt they are the one who raised me to be who I am they always supported my dreams. They always wanted to see me Happy.</p>
            </li>
            <li>
                <img src="images/family.jpg">
                <h3>My Family</h3>
                <p> My Family staying with them has made me the responsible person that I am right now looking forward for many more years to come to spend with them. </p>
            </li>
            <li>
                <img src="images/lil sis.jpg">
                <h3>Siblings</h3>
                <p>I Have two siblings my beautiful sister Gabriela who is now 8 years of age and Nqobile who is 5 years of age.</p>
            </li>
        </ul>
    </section>

</div>

</html>