<link href='https://fonts.googleapis.com/css?family=Leckerli+One|Metrophobic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="birds.css" />

<div class="wrapper">
    <h1>CSS Transitions</h1>
    <div class="wrapper_inner">

        <section class="gallery">

            <div class="gallery_item">

                <span class="gallery_item_preview">
          <a href="#" data-js="1">

            <svg fill="#f55" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>

            <img src="images/john.jpg" alt="" /><span>
            <h3>John Wick</h3>
            <p>This is a must watch movie check it out when you have time</p>

            </span>
                </a>

                </span>

                <div data-lk="1" class="gallery_item_full">
                    <div class="box">
                        <img src="https://25.media.tumblr.com/1adc4029ef3a31124f222add70fa3553/tumblr_n2k1499dIp1st5lhmo1_1280.jpg" alt="" />
                        <h3>Floo</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore rem minima saepe itaque animi fuga consequuntur. Praesentium dolorum neque autem nihil nobis quam animi ullam eos tempora quia eius aliquid?</p>
                    </div>
                </div>
            </div>

            <div class="gallery_item">

                <span class="gallery_item_preview">
          <a href="#" data-js="2">
            <svg fill="#5690F7" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://25.media.tumblr.com/b8fd8b5382cce0e5be44bd1245ea2cf4/tumblr_n2k138YEtG1st5lhmo1_1280.jpg" alt="" /><span>
            <h3>Exmaple Video</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing
              elit.</p>

            </span></a>
                </span>

                <div data-lk="2" class="gallery_item_full">
                    <div class="box">

                        <div class="video">
                            <iframe src="https://www.youtube.com/embed/gLg6qxkQ94A"></iframe>
                        </div>

                        <h3>Example Youtube video</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore rem minima saepe itaque animi fuga consequuntur. Praesentium dolorum neque autem nihil nobis quam animi ullam eos tempora quia eius aliquid?</p>
                    </div>
                </div>
            </div>

            <div class="gallery_item">

                <span class="gallery_item_preview">
          <a href="#" data-js="3">
            <svg fill="#5690F7" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://24.media.tumblr.com/5c73892a883adf41e9b8b7fe807e19b8/tumblr_n2k10foRBd1st5lhmo1_1280.jpg" alt="" /><span>
            <h3>Exmaple Video 2</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing
              elit.</p>

            </span></a>
                </span>

                <div data-lk="3" class="gallery_item_full">
                    <div class="box">
                        <div class="video">
                            <iframe src="//player.vimeo.com/video/89918113"></iframe>
                        </div>
                        <h3>Example Vimeo video</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore rem minima saepe itaque animi fuga consequuntur. Praesentium dolorum neque autem nihil nobis quam animi ullam eos tempora quia eius aliquid?</p>
                    </div>
                </div>
            </div>

            <div class="gallery_item">

                <span class="gallery_item_preview">
          <a href="#" data-js="4">
            <svg fill="#f55" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://25.media.tumblr.com/72ff371588fd911cf7725394740c62a0/tumblr_n2k15mq4xy1st5lhmo1_1280.jpg" alt="" /><span>

            </span></a>
                </span>

                <div data-lk="4" class="gallery_item_full">
                    <div class="box">
                        <img src="https://25.media.tumblr.com/72ff371588fd911cf7725394740c62a0/tumblr_n2k15mq4xy1st5lhmo1_1280.jpg" alt="" />

                    </div>
                </div>
            </div>

            <div class="gallery_item">

                <span class="gallery_item_preview">
          <a href="#" data-js="5">
            <svg fill="#f55" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://25.media.tumblr.com/e615708905cc9df81f901c06ae4afe1e/tumblr_n21ltnVLtz1st5lhmo1_1280.jpg" alt="" /><span>

            </span></a>

                </span>
                <div data-lk="5" class="gallery_item_full">
                    <div class="box">
                        <img src="https://25.media.tumblr.com/e615708905cc9df81f901c06ae4afe1e/tumblr_n21ltnVLtz1st5lhmo1_1280.jpg" alt="" />

                    </div>
                </div>
            </div>

            <div class="gallery_item">
                <span class="gallery_item_preview">
          <a href="#" data-js="6">
            <svg fill="#f55" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://25.media.tumblr.com/6c0e95d27b8509096274f244ff5aeea9/tumblr_n21lusI21V1st5lhmo1_1280.jpg" alt="" /><span>

            </span></a>
                </span>

                <div data-lk="6" class="gallery_item_full">
                    <div class="box">
                        <img src="https://25.media.tumblr.com/6c0e95d27b8509096274f244ff5aeea9/tumblr_n21lusI21V1st5lhmo1_1280.jpg" alt="" />
                        <h3>Example image</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore rem minima saepe itaque animi fuga consequuntur. Praesentium dolorum neque autem nihil nobis quam animi ullam eos tempora quia eius aliquid?</p>
                    </div>
                </div>
            </div>
            <div class="gallery_item">

                <span class="gallery_item_preview">
          <a href="#" data-js="7">
            <svg fill="#f55" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://24.media.tumblr.com/bb592565a7d8c4f004973fe3045cac6f/tumblr_n21lvqbJ2m1st5lhmo1_1280.jpg" alt="" /><span>
            </span></a>
                </span>
                <div data-lk="7" class="gallery_item_full">
                    <div class="box">
                        <img src="https://24.media.tumblr.com/bb592565a7d8c4f004973fe3045cac6f/tumblr_n21lvqbJ2m1st5lhmo1_1280.jpg" alt="" />

                    </div>
                </div>
            </div>

            <div class="gallery_item">
                <span class="gallery_item_preview">
          <a href="#" data-js="8">
            <svg fill="#f55" class="gallery_top" viewbox="0 3 60 20">
              <path d="M 0.65359477,1.3817905 C 60.201925,8.44316 121.92863,11.583451 175.81699,28.832771 l 0.6536,-28.10457531 z"></path>
            </svg>
            <img src="https://25.media.tumblr.com/0a4075c0b36aa37ddbd85d4e75afbeca/tumblr_n21lwpVo3F1st5lhmo1_1280.jpg" alt="" /><span>

            </span></a>
                </span>

                <div data-lk="8" class="gallery_item_full">
                    <div class="box">
                        <img src="https://25.media.tumblr.com/0a4075c0b36aa37ddbd85d4e75afbeca/tumblr_n21lwpVo3F1st5lhmo1_1280.jpg" alt="" />
                        <h3>Example image</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore rem minima saepe itaque animi fuga consequuntur. Praesentium dolorum neque autem nihil nobis quam animi ullam eos tempora quia eius aliquid?</p>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>