<header>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://tympanus.net/Development/HoverEffectIdeas/fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
    <div id="banner">

    </div>

    <nav id="navigation">
        <ul id="nav" class="breadcrumb">
            <?php
                $index = "myButtons";
                $about = "myButtons";
                $gallary = "myButtons";
                $contact = "myButtons";
                $family = "myButtons";
                $designs = "myButtons";

                $menuLinkid = basename($_SERVER['PHP_SELF']);
                if ($menuLinkid=="index.php"){
                    $index='myActiveButton';
                }
                elseif ($menuLinkid == "about.php") {
                $about ='myActiveButton';
                }
                elseif ($menuLinkid=="gallary.php") {
                $gallary='myActiveButton';
                }
                elseif ($menuLinkid=="contact.php") {
               $contact='myActiveButton';
               }
               elseif ($menuLinkid=="book_shelf.php") {
               $family='myActiveButton';
               }
                elseif ($menuLinkid=="animals.php") {
               $designs='myActiveButton';
               }

              ?>

                <li style="list-style: none; display: inline">
                    <a class="<?php echo $index;?> myButtons" href="index.php">Home</a>
                    <a class="<?php echo $about;?> myButtons" href="about.php">About</a>
                    <a class="<?php echo $gallary;?> myButtons" href="gallary.php">Gallery</a>
                    <a class="<?php echo $contact;?> myButtons" href="contact.php">Contact</a>
                    <a class="<?php echo $family;?> myButtons" href="soccer_team.php">Portfolio</a>
                    <a class="<?php echo $designs;?> myButtons" href="animals.php">My Pats</a>
                </li>
        </ul>
    </nav>

</header>