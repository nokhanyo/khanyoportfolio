


var  $grid = $('.grid').isotope({
  itemSelector: '.color',
  layoutMode: 'fitRows'
});
  
$('button').click( function() {
  var filterValue = $( this ).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});